# Sample App : Golang

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

This is a sample application to help you deploy your service on both prod 
and non prod environments. 

> Note: Make sure that AWS Access keys & IDs for both prod and non prod account are stored as CI/CD variables on Gitlab

## Pre-requisites

- [Gitlab] AWS Credentials to gitlab runner
- Config Service Team Token in CI/CD Variables.

## Usage

Use build stage to push the docker image to vedantu-registry.
You can directly use staging_deploy & production_deploy stage from  
gitlab-ci

The image being used is `xxxx.dkr.ecr.ap-south-1.amazonaws.com/ci-images:ci-tools-x.x.x` 
which contains necessary tools like `(initialize_helm.sh, FetchConfig)` to automate the deployment and fetch the application's config from config-service 

```sh
#pass qa profile for staging using -p
initialize_helm.sh -p qa 
#pass prod profile for production
initialize_helm.sh -p prod
#initialize_helm will setup KUBECONFIG according to profiles

FetchConfig
#stores retrieved application's config in values.yaml and apply through helm.
```
Set binary name as the Application name with path.

[Gitlab]: <https://docs.gitlab.com/ee/ci/variables/#custom-variables-validated-by-gitlab>
