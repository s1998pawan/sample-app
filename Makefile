all: clean test build

APP=sample-app

build:
	echo Building "./out/${APP}"...
	mkdir -p ./out
	go build -o "./out/${APP}"

test:
	GO111MODULE=on go test -v -coverprofile coverage.out ./...

