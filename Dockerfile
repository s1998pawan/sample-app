FROM amd64/golang:alpine as builder
RUN apk update && apk add --no-cache git

WORKDIR /src
COPY . ./
RUN	mkdir -p ./out
RUN CGO_ENABLED=0 GOARCH=amd64 GOOS=linux go build -a -installsuffix cgo -o "./out/sample-app" .

FROM amd64/alpine:latest
RUN apk --no-cache add ca-certificates bash
COPY --from=builder /src/out/sample-app /opt/sample-app
ENTRYPOINT ["/opt/sample-app"]

