package main

import (
	"fmt"
	"github.com/briandowns/spinner"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

func MainHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("Hello, This is a Sample App Writen in Golang")
	w.Write([]byte("Hello, This is a Sample App Writen in Golang"))
}
func HelloHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("got ping request version 2.1 from Kubernetes")
	w.Write([]byte("Pong version 2.1!! From Kubernetes"))
}

func HelloHandlerv2(w http.ResponseWriter, r *http.Request) {
	log.Printf("got v1/ping request ")
	w.Write([]byte("Pong V1 !!"))
}

func main() {
	addr := ":8080"

	mux := http.NewServeMux()
	mux.HandleFunc("/", MainHandler)
	mux.HandleFunc("/ping", HelloHandler)
	mux.HandleFunc("/v1/ping", HelloHandlerv2)

	log.Printf("server is listening at %s", addr)

	if len(os.Args) > 1 {
		switch os.Args[1] {
		case "help":
			s := spinner.New(spinner.CharSets[9], 100*time.Millisecond)
			s.Start()
			time.Sleep(4 * time.Second)
			s.Stop()
			fmt.Printf("\n ------------    subcommand 'help' --------------\n")
			fmt.Printf("\n ------------	Usage: ./%s  ---------------\n\n", os.Args[0])
		default:
			fmt.Println("expected 'one' or 'two' subcommands")
			os.Exit(1)
		}
	} else {
		for _, env := range os.Environ() {
			envPair := strings.SplitN(env, "=", 2)
			key := envPair[0]
			value := envPair[1]

			fmt.Printf("%s : %s\n", key, value)
		}
	}

	log.Fatal(http.ListenAndServe(addr, mux))
}
